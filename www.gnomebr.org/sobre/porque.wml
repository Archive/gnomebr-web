<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>
	<title>GNOME-BR</title>
	<style type="text/css">
      	div.textbox  {
        	border-style: solid;
        	background: whitesmoke;
        	color: #000000;
        	border-width: 1px;
       		padding: 2px;
       		margin:15px;
	}
    	</style>
</head>

<body>

<h1>Pourquoi choisir GNOME?</h1>

<p>
	GNOME propose un environnement de travail complet, libre et 
	facile d'utilisation, ainsi qu'une plate-forme de conception 
	de logiciels éprouvée. GNOME inclut en outre :
</p>

<ul>
	<li>Un environnement graphique convivial, configurable et extensible ;</li>
	<li>Des outils bureautiques compatibles avec les standards du marché ;</li>
	<li>Des applications multimédia ;</li>
	<li>Des outils de communication efficaces ;</li>
	<li>Des outils de développement de haut niveau.</li>
</ul>

<p>
   GNOME a été adopté par de nombreux chefs de file de l'industrie 
   informatique comme IBM, Hewlett Packard, ou Sun Microsystems. 
   Des administrations locales et gouvernementales comme la ville 
   de Kenosha (Wisconsin, USA), Extremadura (Espagne), et Pékin 
   (Chine), ont d'ores et déjà choisi GNOME comme environnement 
   de travail.
</p>

<p>
   Un nombre important d'utilisateurs et une grande variété de 
   fournisseurs vous assurent une haute disponibilité de service 
   ainsi qu'une assistance rapide et qualifiée.
</p>

<h2><img src="../img/48x48/gnome-show-desktop.png" alt="Dans les entreprises" />
GNOME pour les entreprises et les administrations</h2>
<div class="textbox">
<p>
   Vous désirez baisser vos coûts de licence et améliorer la 
   fiabilité de vos applications ? GNOME est un choix pertinent. 
   Comme tous les logiciels libres, GNOME peut être installé et 
   utilisé librement et gratuitement quel que soit le nombre de 
   postes. GNOME est simple à installer, à utiliser, à administrer 
   et vous permet de réduire vos coûts de maintenance logicielle.
</p>
</div>

<h2><img src="../img/48x48/gnome-laptop.png" alt="Pour une utilisation personnelle" />
GNOME pour une utilisation personnelle</h2>
<div class="textbox">
<p>
   GNOME est simple à utiliser et facile à appréhender. Il vous 
   apporte tous les logiciels nécessaires à une utilisation 
   quotidienne : suite bureautique compatible avec MS Office ; 
   logiciel de gestion de courrier électronique ; navigateur 
   internet ; messagerie instantanée ; lecteurs de CD, DVD, 
   fichiers musicaux et vidéo ; visioconférence ...
</p>
</div>

<h2><img src="../img/48x48/control-center2.png" alt="Pour les développeurs" />
GNOME pour les développeurs</h2>
<div class="textbox">
<p>
   GNOME est sous licence libre (GPL et LGPL), utilisable pour 
   des développements personnels ou professionnels, pour des 
   logiciels libres ou propriétaires ; la disponibilité de son 
   code source vous permet de l'adapter à vos besoins si nécessaire. 
   De plus les bibliothèques de base de GNOME fonctionnent sous la 
   plupart des Unix ainsi que sous Microsoft Windows et Mac OS X. 
   Enfin, le système d'objets de GNOME peut être exploité dans la 
   plupart des langages de programmation, incluant Java, C#, Python, 
   Perl, C et C++...
</p>
</div>

<h2><img src="../img/48x48/web-browser.png" alt="Pour toutes les langues" />
GNOME pour toutes les langues</h2>
<div class="textbox">
<p>
   Avec sa forte communauté de traducteurs : du Français au Tamoul 
   en passant par le Géorgien, le Breton et l'Espéranto, et son 
   support natif de l'internationalisation, GNOME propose ses 
   applications dans plus de 80 langues.
</p>
</div>

<h2><img src="../img/48x48/accessibility-directory.png" 
alt="Pour les personnes handicapées" />
GNOME pour les personnes handicapées</h2>
<div class="textbox">
<p>
   L'ensemble des fonctionnalités de GNOME est accessible aux 
   personnes souffrant d'un handicap. Elles bénéficient ainsi 
   d'un outil adapté leur permettant d'exercer leur activité 
   professionelle et personnelle, d'utiliser des services, 
   d'accéder à de nombreuses sources d'information dans les 
   meilleures conditions.
</p>
</div>

<p>
Pour télécharger cette page au format PDF, cliquez <a
href="pourquoi_gnome.pdf">ici</a>.
</p>

</body>

</html>
